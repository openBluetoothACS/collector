let mysql = require('mysql');
let config = require('./libs/config');
let express = require('express');
let app = express();
let server = require('http').Server(app);
const bodyParser = require('body-parser');

let connection = mysql.createConnection(config.get('mysql:local'));
const urlencodedParser = bodyParser.urlencoded({extended: false});
const jsonParser = bodyParser.json();

function set_candidate(mac, ip) {
    let sql = "INSERT INTO candidates\n" +
        "\t(mac, ip)\n" +
        "VALUES\n" +
        "\t('" + mac + "', '" + ip + "')\n" +
        "ON DUPLICATE KEY UPDATE\n" +
        "\tip = '" + ip + "'"+
        ",\n\tmac = '" + mac + "'";
    connection.query(sql, function (err, results, fields) {
        if (err) return console.log("Не смог вставить ", err);

    })
}

function toBase(request, response) {

    let mac = request.body.mac;
    let data = request.body.data;
    if (data.length < 1) {
        response.sendStatus(409);
        return;
    }
    console.log('mac: ', mac);
    let sql = mysql.format('SELECT * FROM truks.raspberries WHERE `mac` = ? order by createAt LIMIT 1', [mac]);
    console.log('sql: ', sql);
    connection.query(sql, function (error, raspberry, fields) {
        console.log(raspberry);
        if ((typeof raspberry === 'undefined') || (raspberry.length < 1)) {
            response.sendStatus(401);
            let ip = request.headers['x-forwarded-for'] || request.connection.remoteAddress;
            set_candidate(mac, ip);
            return;
        }

        let raspberryId = raspberry[0].id;

        for (let z in data) {
            data[z].push(raspberryId);
        }
        console.log('data: ', data);
        console.log('we found it');

        connection.beginTransaction(function (err) {
            if (err) {
                throw err;
            }
            connection.query('INSERT INTO rounds (mac, start, end, rssi, raspberryId) VALUES ?', [data], function (error, results, fields) {
                if (error) {
                    return connection.rollback(function () {
                        console.log('Что-то пошло не так');
                        console.log('err: ', error);

                        response.sendStatus(400)
                    });
                }


                connection.commit(function (err) {
                    if (err) {
                        return connection.rollback(function () {
                            // throw err;
                            response.sendStatus(400)
                        });
                    }
                    response.sendStatus(200);
                    console.log('success!');
                });

            });
        });
    });

}

server.listen(config.get('port'), function () {
    console.log('listening on *:' + config.get('port'));
});

app.post('/data', jsonParser, function (req, res) {
    if (!req.body) return res.sendStatus(400);
    console.log('==========');
    console.log('body: ', req.body);
    console.log('==========');
    let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log('ip: ', ip);
    toBase(req, res);
    //res.sendStatus(200);
});